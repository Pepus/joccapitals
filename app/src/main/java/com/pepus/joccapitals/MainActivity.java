package com.pepus.joccapitals;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private FirebaseFirestore db;
    private EditText etEntrada;
    private Button btnPlay;
    private TextView tvnum;
    private static final String TAG = "MyActivity";
    private List<PreguntesiRespostes> llistaPreguntes;
    private String ed_text;
    int valorIntro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etEntrada = findViewById(R.id.etIntro);
        btnPlay = findViewById(R.id.btnPlay);
        tvnum = findViewById(R.id.tvnum);
        db = FirebaseFirestore.getInstance();
        llistaPreguntes = getAllQuestions();

        btnPlay.setVisibility(View.GONE);


    }

    private List<PreguntesiRespostes> getAllQuestions() {
        final List<PreguntesiRespostes> allQuestions = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        db.collection("llistaPreguniResp")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                String pregunta = document.getData().get("pregunta").toString();
                                List<Resposta> resposta = (List<Resposta>) document.getData().get("respostes");
                                Map<String, Double> mapa = (Map<String, Double>) document.getData().get("coordenades");
                                double latitud = mapa.get("latitud");
                                double longitud = mapa.get("longitud");
                                Coordenades coordenades = new Coordenades(latitud, longitud);
                                PreguntesiRespostes pir = new PreguntesiRespostes(pregunta, resposta, coordenades);
                                allQuestions.add(pir);
                            }
                            int numPreg = llistaPreguntes.size();
                            String num = String.valueOf(numPreg);
                            tvnum.setText(num);
                            btnPlay.setVisibility(View.VISIBLE);
                            btnPlay.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ed_text = etEntrada.getText().toString();
                                    if (ed_text.equals("")) {
                                        Toast.makeText(MainActivity.this, "S'ha d'introduïr algún valor", Toast.LENGTH_SHORT).show();
                                    } else {
                                        valorIntro = Integer.parseInt(etEntrada.getText().toString());
                                    }
                                    if (valorIntro == 0) {
                                        Toast.makeText(MainActivity.this, "El valor ha de ser superior a 0", Toast.LENGTH_SHORT).show();
                                    } else if (valorIntro > llistaPreguntes.size()) {
                                        Toast.makeText(MainActivity.this, "El valor introduït supera a les preguntes existents, que són " + llistaPreguntes.size(), Toast.LENGTH_SHORT).show();
                                    } else {
                                        Intent intent = new Intent(MainActivity.this, JocMapa.class);
                                        intent.putExtra("valor", valorIntro);
                                        startActivity(intent);
                                    }
                                }
                            });
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
        return allQuestions;
    }
}
