package com.pepus.joccapitals;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AllQuestionsOK extends AppCompatActivity {

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_questions_o_k);

        TextView textView = findViewById(R.id.tvRespostaFinal);
        Button gameAgain = findViewById(R.id.btnAgain);
        Button exit = findViewById(R.id.btnExit);

        int numRespostes = getIntent().getExtras().getInt("valor");
        int numRespostesTotal = getIntent().getExtras().getInt("valorTotal");

        if (numRespostes == 0) {
            textView.setText(R.string.looser);
        } else if (numRespostes == numRespostesTotal) {
            textView.setText(R.string.tot_ok);
        } else {
            textView.setText(getString(R.string.inici) + String.valueOf(numRespostes) + getString(R.string.final_resposta));
        }


        gameAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(AllQuestionsOK.this, MainActivity.class);
                startActivity(intent);
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AllQuestionsOK.this);
                builder.setTitle(R.string.app_name);
                builder.setIcon(R.mipmap.ic_launcher);
                builder.setMessage(R.string.sure_exit)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finishAffinity();
                                System.exit(0);
                            }
                        })
                        .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }
}
