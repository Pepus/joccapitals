package com.pepus.joccapitals;

public class Coordenades {

    private double latitud;
    private double longitud;

    public Coordenades(double latitud, double longitud) {
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Coordenades() {
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Coordenades{" +
                "latitud=" + latitud +
                ", longitud=" + longitud +
                '}';
    }
}
