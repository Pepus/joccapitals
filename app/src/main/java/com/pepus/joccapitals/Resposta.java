package com.pepus.joccapitals;

public class Resposta {

    private String text;
    private boolean certOfals;

    public Resposta(String text, boolean certOfals) {
        this.text = text;
        this.certOfals = certOfals;
    }

    public Resposta() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isCertOfals() {
        return certOfals;
    }

    public void setCertOfals(boolean certOfals) {
        this.certOfals = certOfals;
    }

    @Override
    public String toString() {
        return "Resposta{" +
                "text='" + text + '\'' +
                ", certOfals=" + certOfals +
                '}';
    }
}
