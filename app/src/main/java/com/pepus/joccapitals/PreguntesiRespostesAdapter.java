package com.pepus.joccapitals;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreguntesiRespostesAdapter extends RecyclerView.Adapter<PreguntesiRespostesAdapter.MyViewHolder> implements View.OnClickListener {

    public List<PreguntesiRespostes> preguntes;
    private View.OnClickListener listener;
    public List<Integer> llistaRespostesSelect;

    public PreguntesiRespostesAdapter(List<PreguntesiRespostes> preguntes) {
        this.preguntes = preguntes;
        this.llistaRespostesSelect = new ArrayList<>(preguntes.size());
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.pregunta, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        v.setOnClickListener(this);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        String pregunta = preguntes.get(position).getPregunta();
        holder.tvPregunta.setText(pregunta);

        List<Resposta> llistaRespostes = preguntes.get(position).getRespostes();
        Map<String, Object> res1 = (Map<String, Object>) llistaRespostes.get(0);
        String resposta1 = (String) res1.get("text");
        holder.resposta1.setText(resposta1);
        holder.resposta1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llistaRespostesSelect.add(position, 0);
            }
        });


        Map<String, Object> res2 = (Map<String, Object>) llistaRespostes.get(1);
        String resposta2 = (String) res2.get("text");
        holder.resposta2.setText(resposta2);
        holder.resposta2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llistaRespostesSelect.add(position, 1);
            }
        });


        Map<String, Object> res3 = (Map<String, Object>) llistaRespostes.get(2);
        String resposta3 = (String) res3.get("text");
        holder.resposta3.setText(resposta3);
        holder.resposta3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llistaRespostesSelect.add(position, 2);
            }
        });


        Map<String, Object> res4 = (Map<String, Object>) llistaRespostes.get(3);
        String resposta4 = (String) res4.get("text");
        holder.resposta4.setText(resposta4);
        holder.resposta4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llistaRespostesSelect.add(position, 3);
            }
        });

    }

    @Override
    public int getItemCount() {
        return preguntes.size();
    }

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null) {
            listener.onClick(v);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvPregunta;
        private RadioGroup radioGroup;
        private RadioButton resposta1;
        private RadioButton resposta2;
        private RadioButton resposta3;
        private RadioButton resposta4;

        public RadioButton getResposta1() {
            return resposta1;
        }

        public RadioButton getResposta2() {
            return resposta2;
        }

        public RadioButton getResposta3() {
            return resposta3;
        }

        public RadioButton getResposta4() {
            return resposta4;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tvPregunta = itemView.findViewById(R.id.tvpregunta);
            this.radioGroup = itemView.findViewById(R.id.rg);
            this.resposta1 = itemView.findViewById(R.id.rb1);
            this.resposta2 = itemView.findViewById(R.id.rb2);
            this.resposta3 = itemView.findViewById(R.id.rb3);
            this.resposta4 = itemView.findViewById(R.id.rb4);

        }
    }
}
