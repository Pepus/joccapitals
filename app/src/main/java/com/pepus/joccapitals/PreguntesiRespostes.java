package com.pepus.joccapitals;

import java.util.List;

public class PreguntesiRespostes {

    private String pregunta;
    private List<Resposta> respostes;
    private Coordenades coordenades;

    public PreguntesiRespostes(String pregunta, List<Resposta> respostes, Coordenades coordenades) {
        this.pregunta = pregunta;
        this.respostes = respostes;
        this.coordenades = coordenades;
    }

    public PreguntesiRespostes() {
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public List<Resposta> getRespostes() {
        return respostes;
    }

    public void setRespostes(List<Resposta> respostes) {
        this.respostes = respostes;
    }

    public Coordenades getCoordenades() {
        return coordenades;
    }

    public void setCoordenades(Coordenades coordenades) {
        this.coordenades = coordenades;
    }

    @Override
    public String toString() {
        return "PreguntesiRespostes{" +
                "pregunta='" + pregunta + '\'' +
                ", respostes=" + respostes +
                ", coordenades=" + coordenades +
                '}';
    }
}
