package com.pepus.joccapitals;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class JocMapa extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private List<PreguntesiRespostes> preguntes;
    private FirebaseFirestore db;
    private PreguntesiRespostesAdapter mAdapter;
    private static final String TAG = "MyActivity";
    private double latitud;
    private double longitud;
    private int numPreguntes;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joc_mapa);
        final Button btnFinish = findViewById(R.id.btnFinish);

        btnFinish.setVisibility(View.GONE);

        recyclerView = findViewById(R.id.rvMyReci);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        numPreguntes = getIntent().getExtras().getInt("valor");
        preguntes = getAllQuestions();

        mAdapter = new PreguntesiRespostesAdapter(preguntes);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                latitud = preguntes.get(recyclerView.getChildAdapterPosition(v)).getCoordenades().getLatitud();
                longitud = preguntes.get(recyclerView.getChildAdapterPosition(v)).getCoordenades().getLongitud();
                updateLocation(latitud, longitud);
            }
        });


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    btnFinish.setVisibility(View.VISIBLE);
                    btnFinish.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            List<Boolean> booleanList = getList();
                            int cont = 0;
                            for (int i = 0; i < booleanList.size(); i++) {
                                if (booleanList.get(i) == Boolean.TRUE) {
                                    cont++;
                                }
                            }
                            Intent intent = new Intent(JocMapa.this, AllQuestionsOK.class);
                            intent.putExtra("valor", cont);
                            intent.putExtra("valorTotal", preguntes.size());
                            startActivity(intent);
                            //Toast.makeText(JocMapa.this, "Has respost " + numResp + " preguntes correctes", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    btnFinish.setVisibility(View.GONE);
                }
            }
        });

    }

    private List<Boolean> getList() {
        List<Boolean> listInside = new ArrayList<>(Arrays.asList(new Boolean[preguntes.size()]));
        Collections.fill(listInside, Boolean.FALSE);
        for (int i = 0; i < mAdapter.llistaRespostesSelect.size(); i++) {

            List<Resposta> llistaRespostes = preguntes.get(i).getRespostes();

            Map<String, Object> res = (Map<String, Object>) llistaRespostes.get(mAdapter.llistaRespostesSelect.get(i));
            boolean boolResp = (boolean) res.get("certOfals");

            listInside.set(i, boolResp);

        }
        return listInside;
    }

    private void updateLocation(double lat, double lon) {
        LatLng newLocation = new LatLng(lat, lon);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(newLocation));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private List<PreguntesiRespostes> getAllQuestions() {
        final List<PreguntesiRespostes> allQuestions = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
        final CollectionReference docRef = db.collection("llistaPreguniResp");
        docRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "Listen failed.", e);
                    return;
                }
                for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                    String pregunta = doc.getData().get("pregunta").toString();
                    List<Resposta> resposta = (List<Resposta>) doc.getData().get("respostes");
                    Map<String, Double> mapa = (Map<String, Double>) doc.getData().get("coordenades");
                    double latitud = mapa.get("latitud");
                    double longitud = mapa.get("longitud");
                    Coordenades coordenades = new Coordenades(latitud, longitud);
                    PreguntesiRespostes pir = new PreguntesiRespostes(pregunta, resposta, coordenades);
                    allQuestions.add(pir);

                }
                int questionsToDelete = allQuestions.size() - numPreguntes;
                int delete = questionsToDelete;
                for (int i = 0; i < questionsToDelete; i++) {
                    int randomInt = (int) (delete * Math.random());
                    allQuestions.remove(randomInt);
                    delete -= 1;
                }
                mAdapter.notifyDataSetChanged();
            }
        });
        return allQuestions;
    }

}
